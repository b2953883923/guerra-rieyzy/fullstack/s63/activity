function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    if (letter.length !== 1) {
        return undefined; 
    }
    
    let count = 0;
    for (let i = 0; i < sentence.length; i++) {
        if (sentence.charAt(i) === letter) {
            count++;
        }
    }
    return count;
    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    const lowerCase = text.toLowerCase();

    const letterMap = {};

    for( let i = 0; i < lowerCase.length; i++) {
        const char = lowerCase[i];
    

    if(/[a-z]/.test(char)){
            if(letterMap[char]){
                return false;
            } else {
                letterMap[char] = true;
            }
        }
    }
    return true;
}

function purchase(age, price) {

    if (age < 13) {
        
        return undefined;
    } else if (age >= 13 && age <= 21 || age >= 65) {
        const discountedPrice = price * 0.8;
        return discountedPrice.toFixed(2);
    } else {
        return price.toFixed(2); 
    }
}


function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.
    const hotCategories = {};

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
    // Loop through each item in the items array.
    for (const item of items) {
        // If the item's stocks are zero, add its category to hotCategories.
        if (item.stocks === 0) {
            hotCategories[item.category] = true;
        }
    }

    // Get the keys (categories) from the hotCategories object and convert them to an array.
     const hotCategoriesArray = Object.keys(hotCategories);

     // Filter out any repeating categories.
     const uniqueHotCategories = [...new Set(hotCategoriesArray)];

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

     return uniqueHotCategories;
};



function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    const votersForA = new Set(candidateA);
    const votersForB = new Set(candidateB);

    const commonVoters = new Set(
    [...votersForA].filter(voter => votersForB.has(voter))
    );

     return Array.from(commonVoters);
}
const candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'];
const candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l'];

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};